# GCP Product Factory


Last PR validation status: ![validation-status](https://concourse.trollab.ca/api/v1/teams/main/pipelines/product-factory/jobs/code-validation/badge)

Last deployment status: ![build-status](https://concourse.trollab.ca/api/v1/teams/main/pipelines/product-factory/jobs/code-deployment/badge)


## Overview

The **GCP Core Product Factory* is a reusable automated toolset used to bootstrap a new Product on GCP. Product is a set of projects in both production and non-production environment that serve the same business purpose.

The code is split into three parts:
- [terraform](./terraform) - contains Infrastructure Code
- [ci](./ci) - contains CI/CD Pipeline configuration
- [products](./products) - contains product variables, one .tfvars file per Product

All adjustments should be made by editing pipeline parameters [ci/parameters.yaml](./ci/parameters.yaml) and Product variables in [products](./products). An example product variables file can be found at [products/default.tfvars](./products/default.tfvars).

The script will create a resource hierarchy enclosing a production and a non-production environment for a single product.

All created projects will be linked to the single Billing Account.

Additionally, the following optional configurations can be performed:

1. Assign project labels
2. Enable APIs
3. Create custom Roles
4. Configure [project-scoped Organization Policies](https://cloud.google.com/resource-manager/docs/organization-policy/understanding-hierarchy#hierarchy_evaluation_rules)
5. Configure IAM roles and permissions both authoritative and additive.
6. Create [project lien](https://cloud.google.com/resource-manager/docs/project-liens) to protect the project from deletion
7. Create a Service Account for automation and assign IAM roles to it.


## Prerequisites

There are few things you should prepare beforehand:

1. GCS bucket for Terraform state storage and a Service Account to access it
2. GCP Service Account credentials with following roles:
    - Folder Creator
    - Project Creator
    - Billing Account User
    - Role Administrator (only if custom IAM roles need to be created)
    - Project IAM Admin (only if IAM roles need to be granted)
    - Organization Policy Administrator (only if Organization Policy need to be enforced)
    - Project Lien Modifier (only if Project Lien is used)


## GitOps Workflow

The following steps should be followed by the team member creating the Product

0. Review the Jira Service Desk Request
1. Clone this repository
2. Create a feature branch named using the following pattern `feature/JIRA_TICKET_NAME/PRODUCT_NAME` for example `feature/CLOUD-16126/topequipe`.
    In case if the code change is not related to a product(i.e. changes in README.md) and should not trigger any terraform actions, the product name must be `noproduct`.
3. Create the Terraform variables file in [./products](./products/) folder with the name `PRODUCT_NAME.tfvars` for example `topequipe.tfvars`.
4. Populate the variables file with data from Service Desk request.
5. Create a new Pull Request to `master` branch to launch the **code validation** Concourse Job and evaluate the deployment plan.
6. After the PR approval process is completed (Communication is key)
7. Merge the Pull Request to `master` branch and Concourse pipeline will deploy the changes automatically.


## BitBucket Webhooks

By default Concourse CI will periodically query BitBucket to get repository status updates. This may result in some delay between BitBucket event and Concourse Build. To make it realtime you can configure BitBucket Webhooks to notify Concourse immediately about significant events, such as push to the branch or creation of a new Pull Request.

You will need admin privileges on a BitBucket repository to configure WebHooks. The procedure is as follows

1. Create two webhooks for infrastructure code repository in BitBucket. The first webhook should be triggered on Push events. Use the following template to construct the URL for this webhook:

      `https://CONCOURSE_HOST/api/v1/teams/TEAM_NAME/pipelines/PIPELINE_NAME/resources/infrastructure-code/check/webhook?webhook_token=WEBHOOK_TOKEN`

    The second webhook should be triggered when PR is Opened, Modified, or Comment was added. Use the following template to construct the URL for this webhook:

      `https://CONCOURSE_HOST/api/v1/teams/TEAM_NAME/pipelines/PIPELINE_NAME/resources/infrastructure-code-pr/check/webhook?webhook_token=WEBHOOK_TOKEN`

    Please keep in mind that production pipeline will have a different Concourse Addresse.

    The `WEBHOOK_TOKEN` is an alpha-numeric `string` that will ensure that Concourse reacts to events coming only from the source that have the token.

2. Put the `WEBHOOK_TOKEN` to the Vault secret manager under key `bitbucket_webhook_token`.

## Pipeline Parameters

Pipeline parameters are set in [parameters.yaml](./ci/parameters.yaml). The pipeline supports the following parameters:

| parameter | description |
---|:---|
|`git_repo_infrastructure`| Infrastructure Code Git URI. |
|`git_branch_infrastructure`| Infrastructure code branch. |
|`bitbucket_username`| BitBucket Username. |
|`bitbucket_password`| BitBucket App Password with access to Read/Write to PR. |
|`bitbucket_webhook_token`| A token used for webhook configuration in BitBucket. |
|`git_repo_modules`| Terraform modules library Git URI. |
|`git_branch_modules`| Terraform modules library branch. |
|`ms_teams_webhook_url`| MS Teams Webhook for notifications. |
|`tf_backend_gcp_credentials`| GCP Service Accout key in JSON format for Terraform Backend. This service account must have storage.editor Role on the GSC bucket set in `backent_tfvars` parameter. |
|`tf_provider_gcp_credentials`| GCP Service Accout key in JSON format form Terraform GCP Provider. This service account must have roles sufficient to deploy infrastructure defined in [`terraform`](./terraform) folder. |
|`backend_tfvars`| Terraform variables for Backend configuration. This is a multiline `string` with terraform variables in [.tfvars format](https://www.terraform.io/docs/configuration/variables.html#variable-definitions-tfvars-files). When using GCS bucket as a backend you will need to set two variables here `bucket`(the bucket name) and `prefix`(folder in the bucket root where state is stored`). |
|`concourse_url` | Concourse URL in formate SCHEME://HOST[:PORT] |
|`vault_url` | Vault URL in formate SCHEME://HOST[:PORT] |
|`vault_role_id` | Vault AppRole Role ID with permissions to write keys. Will publish Service Account keys to the corresponding team prefix. |
|`vault_secret_id` | Vault AppRole Secret ID with permissions to write keys. Will publish Service Account keys to the corresponding team prefix. |
|`vault_concourse_kv_path` | Vault KV secret engine location used by Concourse. |

## Terraform variables
{"constraints/compute.trustedImageProjects" = {inherit_from_parent = null, suggested_value = null, status = true, values = ["projects/my-project"]}}
You can use the following terraform variables in [default.tfvars](./terraform/default.tfvars) to define parameters of created projects.

| Variable | Type | Description | Required |
---|:---|:---|:---:
| `parent_node` | `string` | Folder ID of the parent folder. | :heavy_check_mark: |
| `product_name` | `string` | Name of the product. | :heavy_check_mark: |
| `team_name` | `string` | Service Account keys will be published to Vault under this previx and eventually will be available for the Concourse team. | :heavy_check_mark: |
| `folder_name` | `string` | Folder name used as a composite part of root and environment folders of a product. | :heavy_check_mark: |
| `billing_account` | `string` | Billing account ID. | :heavy_check_mark: |
| `lower_projects` | `map(object(...))` | A map of project name => project specification for project lower environment. | :heavy_check_mark: |
| `upper_projects` | `map(object(...))` | A map of project name => project specification for project upper environment. | :heavy_check_mark: |
| `product_folder_iam_members` | `map(list(string))`| List of IAM members keyed by role(enforced at product folder level).||
| `product_folder_iam_roles` | `list(string)` | List of IAM roles(enforced at product folder level). ||
| `upper_folder_iam_members` | `map(list(string))` | List of IAM members keyed by role(enforced at `upper` folder level). ||
| `upper_folder_iam_roles` | `list(string)` | List of IAM roles(enforced at `upper` folder level). ||
| `lower_folder_iam_members` | `map(list(string))` | List of IAM members keyed by role(enforced at `lower` folder level). ||
| `lower_folder_iam_roles` | `list(string)` | List of IAM roles(enforced at `lower` folder level). ||
| `mandatory_services` | `list(string)` | List of APIs that will be enabled in all created projects. ||
| `service_config` | `map(string)` | Configure service API activation. Defaults will work well for most if not all use cases. ||

Project configuration blocks(`lower_projects` and `upper_projects`) are nested objects with project specifications. Each of those objects is a map of "parameter name" to "parameter configuration spec".
The most basic configuration that will create couple of non prod project and a single prod project(without labels, apis, rbac, etc) looks like this:

```HCL
lower_projects = {
  "cicd-dv" = {
    # ...
  }
  "cicd-qa" = {
    # ...
  }
}
upper_projects = {
  "cicd-pr" = {
    # ...
  }
}
```
The outer map key will be a composit part of project name. The environment qualifier(dv, qa, pr, etc) is optional and can be set as the suffix to the project name.
Please keep in mind that according to the cap set by GCP on project name must be <=30 characters, which means that key name should be <=27 characters.

You can add one or more of the following optional parameters to the inner map:

`labels` - Resource labels that will be set on a project. The example below has a full set of mandatory labels.

```HCL
lower_projects = {
  "cicd-pr" = {
    labels = {
      environnement = "lower"
    }
    # ...
  }
}
```

GCP has following requirements on labels:
- Each label must be a key-value pair.
- Keys must have a minimum length of 1 character and a maximum length of 63 characters, and cannot be empty. Values can be empty, and have a maximum length of 63 characters.
- Keys and values can contain only lowercase letters, numeric characters, underscores, and dashes. All characters must use UTF-8 encoding, and can include international characters.
- The key portion of a label must be unique. However, you can use the same key with multiple resources. Keys must start with a lowercase letter or international character.

`services` - the list of APIs to enable on the per-project basis

```HCL
lower_projects = {
  "cicd-pr" = {
    services = ["cloudresourcemanager.googleapis.com", "iam.googleapis.com"]
    # ...
  }
}
```

`custom_roles` - Custom roles map formatted as a ROLE_NAME => LIST_OF_PERMISSIONS

```HCL
lower_projects = {
  "cicd-pr" = {
    custom_roles = {
      vaultAutoUnsealer = [
        "cloudkms.cryptoKeyVersions.useToEncrypt",
        "cloudkms.cryptoKeyVersions.useToDecrypt",
        "cloudkms.cryptoKeys.get"
      ],
    # ...
  }
}
```

`iam_roles` - List of roles used to set authoritative bindings.

```HCL
lower_projects = {
  "cicd-pr" = {
    iam_roles = ["roles/owner", "roles/editor"]
    # ...
  }
}
```

`iam_members` - Map of member lists used to set authoritative bindings, keyed by role.

```HCL
lower_projects = {
  "cicd-pr" = {
    iam_members = {
      "roles/owner" = ["user:john.smith@yourorganization.com"],
      "roles/editor" = [
        "group:gcp-admins@yourorganization.com",
        "serviceAccount:sa_name@project_id.iam.gserviceaccount.com",
      ],
    }
    # ...
  }
}
```

`iam_additive_bindings` - Map of roles lists used to set non authoritative bindings, keyed by members.

```HCL
lower_projects = {
  "cicd-pr" = {
    iam_additive_bindings = {
      "user:john.smith@yourorganization.com" = [ "roles/owner", ],
      "group:gcp-admins@yourorganization.com" = [ "roles/editor", ],
      "serviceAccount:sa_name@project_id.iam.gserviceaccount.com" = [ "roles/editor", ],
    }
    # ...
  }
}
```

`lien_reason` - If non-empty, creates a project lien with this description.

```HCL
lower_projects = {
  "cicd-pr" = {
    lien_reason = "Mission critical project"
    # ...
  }
}
```

`service_account_roles` - a list of roles that will be granted to the automation service account in the corresponding project.

```HCL
lower_projects = {
  "cicd-pr" = {
    service_account_roles = [
      "roles/logging.logWriter",
      "roles/monitoring.metricWriter",
    ]
    # ...
  }
}
```

### Complete example

Below is an example of a non production project with all possible configuration parameters used.

```HCL
lower_projects = {
  "cicd-dv" = {
    labels = {
      environnement = "lower"
    }
    services = ["cloudresourcemanager.googleapis.com", "iam.googleapis.com"]
    custom_roles = {
      vaultAutoUnsealer = [
        "cloudkms.cryptoKeyVersions.useToEncrypt",
        "cloudkms.cryptoKeyVersions.useToDecrypt",
        "cloudkms.cryptoKeys.get"
      ],
    }
    iam_roles = ["roles/owner", "roles/editor"]
    iam_members = {
      "roles/owner" = ["user:john.smith@yourorganization.com"],
      "roles/editor" = [
        "group:gcp-admins@yourorganization.com",
        "serviceAccount:sa_name@project_id.iam.gserviceaccount.com",
      ],
    }
    iam_additive_bindings = {
      "user:john.smith@yourorganization.com" = [ "roles/owner", ],
      "group:gcp-admins@yourorganization.com" = [ "roles/editor", ],
      "serviceAccount:sa_name@project_id.iam.gserviceaccount.com" = [ "roles/editor", ],
    }
    lien_reason = "Mission critical project"
    service_account_roles = [
      "roles/logging.logWriter",
      "roles/monitoring.metricWriter",
    ]
  }
}
```
