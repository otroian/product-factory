#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

# retrieve product name from validation step
export $(cat ../product-name/keyval.properties | grep PRODUCT)
echo Product name is \"${PRODUCT}\"
if [ -z "${PRODUCT}" ]; then
    echo "Could not detect the product name. Valid branch name is:"
    echo "BRANCH_TYPE/JIRA_TICKET/PRODUCT_NAME"
    echo "For example: feature/CLOUD-16126/product-xyz"
    exit 1
fi

# detect changes that should not trigger terraform action
if [ "${PRODUCT}" = "noproduct" ]; then
    touch ../service-account-keys/skip-publish
    echo "This change is not related to a product"
    echo "Skip terraform actions"
    exit 0
fi

# move concourse parameters into files for terraform consumption
cd ./terraform
echo "${TF_BACKEND_VARIABLES}" > backend.tfvars
echo "${TF_BACKEND_GCP_CREDENTIALS}" > backend_credentials.json
echo "${TF_PROVIDER_GCP_CREDENTIALS}" > credentials.json

echo "==> Init <=="
terraform init \
    -input=false \
    -backend-config=backend.tfvars

echo "==> Prepare workspace <=="
(terraform workspace select ${PRODUCT} 2>/dev/null || terraform workspace new ${PRODUCT})

echo "==> Apply <=="
terraform apply \
    -auto-approve \
    -input=false \
    -var-file=../variables/${PRODUCT}.tfvars

echo "==> Processing outputs <=="
terraform output team_name > ../../service-account-keys/team_name
terraform output -json upper_service_account_keys > ../../service-account-keys/upper_service_account_keys.json
terraform output -json lower_service_account_keys > ../../service-account-keys/lower_service_account_keys.json
echo "==> Done <=="
