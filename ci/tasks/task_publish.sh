#!/bin/bash

set -o errexit
set -o pipefail

exec 3>&1 # make stdout available as fd 3
exec 1>&2 # redirect all output to stderr for logging

export TMPDIR=${TMPDIR:-/tmp}

tmp_file() {
  mktemp "$TMPDIR/vault-$1.XXXXXX"
}

log() {
  # $1: message
  local message="$(date -u '+%F %T') - $1"
  echo -e "$message" >&2
}

vault_request() {
  # URI
  # $1 host
  # $2 path
  # $3 data
  # $4 Vault token, don't use for login requests
  # $5 HTTP Method (default: POST for data, GET without data)
  # $6 Skip SSL verification
  local host="$1"
  local path="$2"
  local data="$3"
  local token="$4"
  local method="$5"
  local skip_ssl_verification=${6:-"false"}
  local api_path="v1"

  local request_url="$1/$api_path/$2"
  local request_result="$(tmp_file result)"
  local request_data="$(tmp_file data)"

  request_result_cleanup() {
    rm -f "$request_result"
    rm -f "$request_data"
  }

  trap request_result_cleanup EXIT

  local extra_options=""

  if [ -n "$data" ]; then
    method=${method:-POST}
    echo "$data" | jq '.' > "$request_data"
    extra_options="-H \"Content-Type: application/json\" -d @\"$request_data\""
  fi

  if [ -n "$method" ]; then
    extra_options+=" -X $method"
  fi

  if [ "$skip_ssl_verification" = "true" ]; then
    extra_options+=" -k"
  fi

  if [ -n "$token" ]; then
    extra_options+=" -H \"X-Vault-Token: $token\""
  fi

  curl_cmd="curl -s $extra_options -o \"$request_result\" \"$request_url\""

  if ! eval $curl_cmd; then
    log "Vault request $request_url failed"
    exit 1
  fi

  errors="$(jq -c ".errors" $request_result)"

  if [ "$errors" == "" ] || [ "$errors" == "null" ];  then
    jq -c '.' $request_result
  else
    log "Error:\n $(jq '.errors' $request_result)"
    exit 1
  fi
  request_result_cleanup
}

vault_login() {
  # $1 host
  # $2 path
  # $3 data
  log "Log in to Vault..."
  vault_request "$1" "$2" "$3" "" "" | jq -r ".auth.client_token"
}

vault_put_to_kv() {
  # $1 host
  # $2 path
  # $3 data
  # $4 token
  vault_request "$1" "$2" "$3" "$4" ""
}

publish_keys_to_vault() {
  # $1 Vault URI
  # $2 Vault path
  # $3 Vault AppRole Role ID
  # $4 Vault AppRole Secret ID
  # $5 Path to the file with SA keys to upload
  local host=$1
  local path=$2
  local role_id=$3
  local secret_id=$4
  local keys_file=$5

  token=$(vault_login "$host" "auth/approle/login" "{\"role_id\":\"$role_id\",\"secret_id\":\"$secret_id\"}")
  service_account_keys="$(cat $keys_file | jq -r '.[] | [.name, .key]|@tsv')"
  while IFS=$'\t' read -r name key; do
    decoded_key=$(echo $key | base64 -d | tr -d '\n' | sed 's+"+\\"+g' | sed 's+\\n+\\\\n+g')
    log "Writing to $host/$path/$name..."
    result="$(vault_put_to_kv "$host" "$path/$name" "{\"value\":\"$decoded_key\"}" "$token")"
    log "Done"
  done <<< "$service_account_keys"
}

if [ -f "./service-account-keys/skip-publish" ]; then
  log "This change is not related to a product.\nNothing to publish."
  exit 0
fi

CONCOURSE_TEAM_NAME="$(cat ./service-account-keys/team_name)"

if [ -z "$VAULT_URL" ]; then
  log "Missing \"VAULT_URL\""
  exit 1
fi

if [ -z "$VAULT_CONCOURSE_KV_PATH" ]; then
  log "Missing \"VAULT_CONCOURSE_KV_PATH\""
  exit 1
fi

if [ -z "$CONCOURSE_TEAM_NAME" ]; then
  log "Missing \"CONCOURSE_TEAM_NAME\""
  exit 1
fi

if [ -z "$VAULT_ROLE_ID" ]; then
  log "Missing \"VAULT_ROLE_ID\""
  exit 1
fi

if [ -z "$VAULT_SECRET_ID" ]; then
  log "Missing \"VAULT_SECRET_ID\""
  exit 1
fi

# publish lower keys if there are any
if [ -n "$(jq -c '.[]' ./service-account-keys/lower_service_account_keys.json)" ]; then
  log "Write lower projects keys to Vault..."
  publish_keys_to_vault ${VAULT_URL} ${VAULT_CONCOURSE_KV_PATH}/${CONCOURSE_TEAM_NAME}/lower ${VAULT_ROLE_ID} ${VAULT_SECRET_ID} ./service-account-keys/lower_service_account_keys.json
fi
# publish upper keys if there are any
if [ -n "$(jq -c '.[]' ./service-account-keys/upper_service_account_keys.json)" ]; then
  log "Write upper projects keys to Vault..."
  publish_keys_to_vault ${VAULT_URL} ${VAULT_CONCOURSE_KV_PATH}/${CONCOURSE_TEAM_NAME}/upper ${VAULT_ROLE_ID} ${VAULT_SECRET_ID} ./service-account-keys/upper_service_account_keys.json
fi
log "All Done."
