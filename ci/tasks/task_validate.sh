#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

# retrieve product name from branch name
PRODUCT=$(git config --get pullrequest.from | cat | cut -d '/' -f 3)
echo PRODUCT="${PRODUCT}" > ../product/keyval.properties
echo Product name is \"${PRODUCT}\"
if [ -z "${PRODUCT}" ]; then
    echo "Could not detect the product name. Valid branch name is:"
    echo "BRANCH_TYPE/JIRA_TICKET/PRODUCT_NAME"
    echo "For example: feature/CLOUD-12345/product-xyz"
    exit 1
fi

# detect changes that should not trigger terraform action
if [ "${PRODUCT}" = "noproduct" ]; then
    echo "This change is not related to a product"
    echo "Skip terraform actions"
    exit 0
fi

# move concourse parameters into files for terraform consumption
cd ./terraform
echo "${TF_BACKEND_VARIABLES}" > backend.tfvars
echo "${TF_BACKEND_GCP_CREDENTIALS}" > backend_credentials.json
echo "${TF_PROVIDER_GCP_CREDENTIALS}" > credentials.json

echo "==> Init <=="
terraform init \
    -input=false \
    -backend-config=backend.tfvars

echo "==> Validate <=="
terraform validate

echo "==> Prepare workspace <=="
(terraform workspace select ${PRODUCT} 2>/dev/null || terraform workspace new ${PRODUCT})

echo "==> Plan <=="
terraform plan \
    -input=false \
    -var-file=../variables/${PRODUCT}.tfvars

echo "==> Done <=="
