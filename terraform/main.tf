locals {
  prefix              = var.prefix == null ? "" : "${var.prefix}-"
  product_folder_name = "${local.prefix}${var.product_name}"
  upper_prefix        = "${local.product_folder_name}-upr"
  lower_prefix        = "${local.product_folder_name}-lwr"
}

resource "random_string" "suffix" {
  count   = length(var.lower_projects) + length(var.lower_projects) == 0 ? 0 : 1
  length  = 2
  upper   = false
  special = false
}

module "product_root" {
  source         = "../../terraform-modules/modules/folders"
  parent         = "folders/${var.parent_node}"
  names          = [ local.product_folder_name ]
  iam_members    = map(local.product_folder_name, var.product_folder_iam_members)
  iam_roles      = map(local.product_folder_name, var.product_folder_iam_roles)
  policy_boolean = var.product_folder_policy_boolean
  policy_list    = var.product_folder_policy_list
}

module "upper_folder" {
  source         = "../../terraform-modules/modules/folders"
  parent         = module.product_root.id
  names          = [ local.upper_prefix ]
  iam_members    = map(local.upper_prefix, var.upper_folder_iam_members)
  iam_roles      = map(local.upper_prefix, var.upper_folder_iam_roles)
  policy_boolean = var.upper_folder_policy_boolean
  policy_list    = var.upper_folder_policy_list
}

module "lower_folder" {
  source         = "../../terraform-modules/modules/folders"
  parent         = module.product_root.id
  names          = [ local.lower_prefix ]
  iam_members    = map(local.lower_prefix, var.lower_folder_iam_members)
  iam_roles      = map(local.lower_prefix, var.lower_folder_iam_roles)
  policy_boolean = var.lower_folder_policy_boolean
  policy_list    = var.lower_folder_policy_list
}

module "lower_projects" {
  source                = "../../terraform-modules/modules/project"
  for_each              = var.lower_projects
  parent                = module.lower_folder.ids_list[0]
  billing_account       = var.billing_account
  prefix                = local.lower_prefix
  name                  = join("-", [each.key, random_string.suffix.0.result])
  services              = concat(var.mandatory_services, lookup(each.value, "services", []))
  labels                = lookup(each.value, "labels", {})
  custom_roles          = lookup(each.value, "custom_roles", {})
  iam_additive_bindings = lookup(each.value, "iam_additive_bindings", {})
  iam_roles             = lookup(each.value, "iam_roles", [])
  iam_members           = lookup(each.value, "iam_members", {})
  policy_boolean        = lookup(each.value, "policy_boolean", {})
  policy_list           = lookup(each.value, "policy_list", {})
  lien_reason           = lookup(each.value, "lien_reason", "")
  service_config        = var.service_config
}

module "upper_projects" {
  source                = "../../terraform-modules/modules/project"
  for_each              = var.upper_projects
  parent                = module.upper_folder.ids_list[0]
  billing_account       = var.billing_account
  prefix                = local.upper_prefix
  name                  = join("-", [each.key, random_string.suffix.0.result])
  services              = concat(var.mandatory_services, lookup(each.value, "services", []))
  labels                = lookup(each.value, "labels", {})
  custom_roles          = lookup(each.value, "custom_roles", {})
  iam_additive_bindings = lookup(each.value, "iam_additive_bindings", {})
  iam_roles             = lookup(each.value, "iam_roles", [])
  iam_members           = lookup(each.value, "iam_members", {})
  policy_boolean        = lookup(each.value, "policy_boolean", {})
  policy_list           = lookup(each.value, "policy_list", {})
  lien_reason           = lookup(each.value, "lien_reason", "")
  service_config        = var.service_config
}

module "lower_service_accounts" {
  source     = "./modules/service-accounts"
  depends_on = [module.lower_projects]
  for_each   = var.lower_projects
  project_id = join("-", [local.lower_prefix, each.key, random_string.suffix.0.result])
  account_id = join("-", [local.lower_prefix, "sa-terraform"])
  iam_roles  = lookup(each.value, "service_account_roles", [])
}

module "upper_service_accounts" {
  source     = "./modules/service-accounts"
  depends_on = [module.upper_projects]
  for_each   = var.upper_projects
  project_id = join("-", [local.upper_prefix, each.key, random_string.suffix.0.result])
  account_id = join("-", [local.upper_prefix, "sa-terraform"])
  iam_roles  = lookup(each.value, "service_account_roles", [])
}
