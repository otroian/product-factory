output "service_account_key" {
  value = {
    "name" = "gcp-sa-${var.project_id}"
    "key"  = google_service_account_key.default.private_key
  }
}
