variable "project_id" {
  type        = string
  description = "Project ID."
}

variable "account_id" {
  type        = string
  description = "Service Account ID"
  default     = "automation"
}

variable "display_name" {
  type        = string
  default     = "Terraform Service Account"
  description = "Service Account display name. Defaults to `Terraform Service Account`."
}

variable "description" {
  type        = string
  default     = "Programmatic Service Account for Terraform deployments"
  description = "Service Account description. Defaults to `Programmatic Service Account for Terraform deployments`."
}

variable "iam_roles" {
  type        = list(string)
  default     = []
  description = "The list of IAM role to grant to the Service Account."
}
