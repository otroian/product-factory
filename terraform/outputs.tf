locals {
  upper_service_account_keys = [for key, value in module.upper_service_accounts: value.service_account_key]
  lower_service_account_keys = [for key, value in module.lower_service_accounts: value.service_account_key]
}

output "team_name" {
  value = var.team_name
}

output "upper_service_account_keys" {
  value     = local.upper_service_account_keys
  sensitive = true
}

output "lower_service_account_keys" {
  value     = local.lower_service_account_keys
  sensitive = true
}
