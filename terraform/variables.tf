variable "prefix" {
  type        = string
  description = "Resource name prefix."
  default     = null
}

variable "product_name" {
  type        = string
  description = "Name of the product."
}

variable "team_name" {
  type        = string
  description = "Name of the team as in Concourse."
}

variable "parent_node" {
  type        = string
  description = "Folder ID of the parent folder."
}

variable "billing_account" {
  type        = string
  description = "Billing account ID."
}

variable "mandatory_services" {
  type        = list(string)
  description = "List of APIs that will be enabled in all created projects."
  default     = []
}

variable "lower_projects" {
  description = "A map of project name => project configuration object for the non production environment."
}

variable "upper_projects" {
  description = "A map of project name => project configuration object for the production environment."
}

variable "service_config" {
  type        = map(string)
  description = "Configure service API activation."
  default = {
    disable_on_destroy         = false
    disable_dependent_services = false
  }
}

variable "product_folder_iam_members" {
  type        = map(list(string))
  description = "List of IAM members keyed by role(enforced at product folder level)."
  default     = null
}

variable "product_folder_iam_roles" {
  type        = list(string)
  description = "List of IAM roles(enforced at product folder level)."
  default     = []
}

variable "product_folder_policy_boolean" {
  description = "Map of boolean org policies and enforcement value(enforced at product folder level). Set value to null for policy restore."
  type        = map(bool)
  default     = {}
}

variable "product_folder_policy_list" {
  description = "Map of list org policies(enforced at product folder level), status is true for allow, false for deny, null for restore. Values can only be used for allow or deny."
  type = map(object({
    inherit_from_parent = bool
    suggested_value     = string
    status              = bool
    values              = list(string)
  }))
  default = {}
}

variable "upper_folder_iam_members" {
  type        = map(list(string))
  description = "List of IAM members keyed by role(enforced at `prod` folder level)."
  default     = null
}

variable "upper_folder_iam_roles" {
  type        = list(string)
  description = "List of IAM roles(enforced at `prod` folder level)."
  default     = []
}

variable "upper_folder_policy_boolean" {
  description = "Map of boolean org policies and enforcement value(enforced at `prod` folder level), set value to null for policy restore."
  type        = map(bool)
  default     = {}
}

variable "upper_folder_policy_list" {
  description = "Map of list org policies(enforced at `prod` folder level), status is true for allow, false for deny, null for restore. Values can only be used for allow or deny."
  type = map(object({
    inherit_from_parent = bool
    suggested_value     = string
    status              = bool
    values              = list(string)
  }))
  default = {}
}

variable "lower_folder_iam_members" {
  type        = map(list(string))
  description = "List of IAM members keyed by role(enforced at `prod` folder level)."
  default     = null
}

variable "lower_folder_iam_roles" {
  type        = list(string)
  description = "List of IAM roles(enforced at `prod` folder level)."
  default     = []
}

variable "lower_folder_policy_boolean" {
  description = "Map of boolean org policies and enforcement value(enforced at `nprod` folder level), set value to null for policy restore."
  type        = map(bool)
  default     = {}
}

variable "lower_folder_policy_list" {
  description = "Map of list org policies(enforced at `nprod` folder level), status is true for allow, false for deny, null for restore. Values can only be used for allow or deny."
  type = map(object({
    inherit_from_parent = bool
    suggested_value     = string
    status              = bool
    values              = list(string)
  }))
  default = {}
}
