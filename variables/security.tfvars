parent_node     = "832700837262"
team_name       = "security"
product_name    = "security"
billing_account = "014E97-5E8F98-8CF653"
mandatory_services = ["cloudresourcemanager.googleapis.com",]
lower_projects = {
  "vault" = {
    labels = {
        environment = "lower"
    },
    service_account_roles = ["roles/storage.admin", "roles/compute.admin"]
  }
  "cicd" = {
    labels = {
        environment = "lower"
    },
    service_account_roles = ["roles/storage.admin", "roles/compute.admin"]
  }
}
upper_projects = {
  "vault" = {
    labels = {
        environment = "upper"
    },
    service_account_roles = ["roles/storage.admin", "roles/compute.admin"]
  }
  "cicd" = {
    labels = {
        environment = "upper"
    },
    service_account_roles = ["roles/storage.admin", "roles/compute.admin"]
  }
}
